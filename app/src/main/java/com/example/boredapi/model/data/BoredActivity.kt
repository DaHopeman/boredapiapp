package com.example.boredapi.model.data

data class BoredActivity(
    val activity: String = "",
    val type: ActivityType = ActivityType.ANY,
    val participants: Int = -1,
    val price: Double = -1.0,
    val key: Int = -1,
    val accessibility: Double = -1.0,
    val link: String = "",
    val error: String = ""
)
