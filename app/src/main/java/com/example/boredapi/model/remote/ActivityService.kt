package com.example.boredapi.model.remote

import com.example.boredapi.model.data.BoredActivity
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ActivityService {

    @GET("activity")
    fun getActivity(@Query("type") type: String?,
                            @Query("participants") participants: Int?
    ): Call<BoredActivity>

    companion object {

        private const val BASE_URL = "https://www.boredapi.com/api/"
        private var instance: ActivityService? = null

        fun getInstance(): ActivityService {
            if (instance == null) {
                val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

                val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .retryOnConnectionFailure(false)
                    .build()

                instance = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ActivityService::class.java)
            }

            return instance!!
        }
    }
}