package com.example.boredapi.model.data

import com.google.gson.annotations.SerializedName

//All activity types according to the BoredApi Documentation (https://www.boredapi.com/documentation)
enum class ActivityType(val printableName: String) {
    ANY("Any"),
    @SerializedName("education")
    EDUCATION("Education"),
    @SerializedName("recreational")
    RECREATIONAL("Recreational"),
    @SerializedName("social")
    SOCIAL("Social"),
    @SerializedName("diy")
    DIY("Diy"),
    @SerializedName("charity")
    CHARITY("Charity"),
    @SerializedName("cooking")
    COOKING("Cooking"),
    @SerializedName("relaxation")
    RELAXATION("Relaxation"),
    @SerializedName("music")
    MUSIC("Music"),
    @SerializedName("busywork")
    BUSYWORK("Busywork")
}