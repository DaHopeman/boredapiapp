package com.example.boredapi.model.repository

import com.example.boredapi.model.data.ActivityType
import com.example.boredapi.model.remote.ActivityService

//Repository for Adaptability
class ActivityRepository {

    private val activityService = ActivityService.getInstance()

    fun getFilteredActivity(type: ActivityType?,
                                    participants: Int?
    ) = activityService.getActivity(type?.name?.lowercase(), participants)
}