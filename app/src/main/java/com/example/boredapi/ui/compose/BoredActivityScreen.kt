package com.example.boredapi.ui.compose

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.boredapi.model.data.ActivityType
import com.example.boredapi.model.data.BoredActivity
import com.example.boredapi.viewmodel.ActivityEvent
import com.example.boredapi.viewmodel.ActivityViewModel
import com.example.boredapi.viewmodel.UiEvent

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BoredActivityScreen(
    viewModel: ActivityViewModel = viewModel(),
    snackbarHostState: SnackbarHostState = remember { SnackbarHostState()}
) {
    val state = viewModel.uiState.value
    LaunchedEffect(snackbarHostState) {
        viewModel.activityEvent.collect {event ->
            when (event) {
                is ActivityEvent.Success -> {
                    //snackbarHostState.showSnackbar("New Activity loaded")
                }
                is ActivityEvent.Error -> {
                    snackbarHostState.showSnackbar(event.message)
                }
            }
        }
    }
    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState)},
        content = { scaffoldPadding ->
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(scaffoldPadding)
            ) {
                if (state.isLoading) {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier.fillMaxSize()
                    ) {
                        CircularProgressIndicator()
                    }
                }
                else {
                    HeaderMessage()

                    if (state.activity.activity != "") {
                        ActivityCard(activity = state.activity)

                        Spacer(modifier = Modifier.height(40.dp))
                    }

                    TypeDropdown(
                        value = state.filterType,
                        expanded = state.isTypeMenuOpen,
                        onExpandedChange = { expanded ->
                            viewModel.onEvent(UiEvent.FilterTypeMenuClicked(expanded))
                        },
                        onTypeChanged = { type -> viewModel.onEvent(UiEvent.FilterTypeChanged(type)) },
                    )
                    ParticipantsInput(value = state.filterParticipants,
                        isError = state.filterParticipantsError,
                        onValueChange = { input ->
                            viewModel.onEvent(
                                UiEvent.FilterParticipantsChanged(
                                    input
                                )
                            )
                        }
                    )

                    Spacer(Modifier.height(30.dp))

                    ConfirmButton {
                        viewModel.onEvent(UiEvent.ButtonClicked)
                    }
                }
            }
        }
    )
}

@Composable
private fun ActivityCard(
    activity: BoredActivity
) {
    Card(modifier = Modifier
        .fillMaxWidth()
        .padding(8.dp)
    ) {
        Column(modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
        ) {
            CardText(
                text = activity.activity,
                fontSize = 23.sp,
                fontWeight = FontWeight.Bold
            )
            CardText(
                text = "This activity is for ${activity.participants} " +
                        if (activity.participants > 1) "People" else "Person"
            )
            CardText(text = "Type: ${activity.type.printableName}")
            if (activity.price == 0.0)
                CardText(text = "This activity is free of charge!")
            else
                CardText(text = "Price: ${activity.price}")
            CardText(text = "Accessibility: ${activity.accessibility}")
            if (activity.link != "")
                LinkText(url = activity.link)
        }
    }
}

@Composable
private fun CardText(
    text: String,
    fontSize: TextUnit = 16.sp,
    fontWeight: FontWeight = FontWeight.Normal
) {
    Text(
        text = text,
        modifier = Modifier.fillMaxWidth(),
        textAlign = TextAlign.Center,
        fontSize = fontSize,
        fontWeight = fontWeight
    )
}

@Composable
private fun LinkText(
    url: String
) {
    val annotatedText = buildAnnotatedString {
        append("Click ")
        pushStringAnnotation(tag = "URL", annotation = url)
        withStyle(style = SpanStyle(
            color = MaterialTheme.colorScheme.primary,
            textDecoration = TextDecoration.Underline
        )) {
            append("this link")
        }
        pop()
        append(" to go to the website")
    }

    val uriHandler = LocalUriHandler.current

    ClickableText(
        text = annotatedText,
        modifier = Modifier.fillMaxWidth(),
        style = TextStyle(
            fontSize = 16.sp,
            textAlign = TextAlign.Center,
            color = MaterialTheme.colorScheme.contentColorFor(MaterialTheme.colorScheme.background)
        ),
        onClick = { offset ->
            annotatedText.getStringAnnotations(
                tag = "URL",
                start = offset,
                end = offset
            ).firstOrNull()?.let {stringAnnotation ->
                Log.d("Link", stringAnnotation.item)
                uriHandler.openUri(stringAnnotation.item)
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun ParticipantsInput(
    value: String,
    isError: Boolean,
    onValueChange: (String) -> Unit
) {
    val focusManager = LocalFocusManager.current
    OutlinedTextField(
        value = value,
        onValueChange = {input -> onValueChange(input)},
        keyboardOptions = KeyboardOptions.Default.copy(
            keyboardType = KeyboardType.NumberPassword
        ),
        keyboardActions = KeyboardActions(
            onDone = {focusManager.clearFocus()}
        ),
        label = {Text("Participants")},
        placeholder = {Text("Any")},
        singleLine = true,
        isError = isError,
        modifier = Modifier.fillMaxWidth(),
    )
}

@Composable
private fun ConfirmButton(
    onClick: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Button(
            onClick = onClick
        ) {
            Text(text = "Get a new activity")
        }
    }
}

@Preview
@Composable
private fun ButtonPreview() {
    ConfirmButton(onClick = {})
}

@Preview(showBackground = true)
@Composable
private fun HeaderMessage() {
    Text(
        text = "Bored? You could...",
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp),
        textAlign = TextAlign.Center,
        fontSize = 30.sp,
        fontWeight = FontWeight.Bold
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun TypeDropdown(
    value: ActivityType,
    expanded: Boolean,
    onExpandedChange: (Boolean) -> Unit,
    onTypeChanged: (ActivityType) -> Unit
) {
    val items = ActivityType.values()
    //Dropdown Box
    ExposedDropdownMenuBox(expanded = expanded, onExpandedChange = onExpandedChange) {
        //Text Field - Displays the selected item
        TextField(
            value = value.printableName,
            onValueChange = {},
            readOnly = true,
            label = { Text(text = "Type") },
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
            },
            colors = ExposedDropdownMenuDefaults.textFieldColors(),
            modifier = Modifier
                .fillMaxWidth()
                .menuAnchor()
        )
        //Dropdown Menu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { onExpandedChange(false) },
            modifier = Modifier.fillMaxWidth()
        ) {
            items.forEach {
                DropdownMenuItem(
                    onClick = { onTypeChanged(it) },
                    text = { Text(text = it.printableName) }
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ParticipantsInputPreview() {
    var number by rememberSaveable { mutableStateOf("") }
    val change: (String) -> Unit = {
        number = it
    }
    ParticipantsInput(value = number, isError = false, onValueChange = change)
}

@Preview
@Composable
private fun ActivityCardPreview() {
    ActivityCard(activity = BoredActivity(
        activity = "Create a Compose Preview",
        type = ActivityType.EDUCATION,
        participants = 1,
        price = 0.0,
        key = 0,
        accessibility = 0.0,
        link = "https://www.jetpackcompose.net/jetpack-compose-preview"
    ))
}