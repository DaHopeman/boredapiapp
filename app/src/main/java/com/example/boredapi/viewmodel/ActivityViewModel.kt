package com.example.boredapi.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.boredapi.model.data.ActivityType
import com.example.boredapi.model.data.BoredActivity
import com.example.boredapi.model.repository.ActivityRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class BoredActivityUiState(
    val activity: BoredActivity = BoredActivity(),
    val filterType: ActivityType = ActivityType.ANY,
    val filterParticipants: String = "",
    val isLoading: Boolean = false,
    val isTypeMenuOpen: Boolean = false,
    val filterParticipantsError: Boolean = false
)

sealed class UiEvent {
    data class FilterTypeChanged(val type: ActivityType): UiEvent()
    data class FilterParticipantsChanged(val participants: String): UiEvent()
    data class FilterTypeMenuClicked(val expanded: Boolean): UiEvent()
    object ButtonClicked: UiEvent()
}

sealed class ActivityEvent {
    object Success: ActivityEvent()
    data class Error(val message: String): ActivityEvent()
}

class ActivityViewModel: ViewModel() {
    private val repo = ActivityRepository()

    private val _uiState = mutableStateOf(BoredActivityUiState())
    val uiState: State<BoredActivityUiState> = _uiState
    val activityEvent = MutableSharedFlow<ActivityEvent>()

    init {
        getActivity()
    }

    fun onEvent(event: UiEvent) {
        when(event) {
            is UiEvent.FilterTypeChanged -> {
                _uiState.value = _uiState.value.copy(filterType = event.type, isTypeMenuOpen = false)
            }
            is UiEvent.FilterParticipantsChanged -> {
                val hasError = event.participants.isNotEmpty() && !event.participants.isDigitsOnly()
                _uiState.value = _uiState.value.copy(filterParticipants = event.participants, filterParticipantsError = hasError)
            }
            is UiEvent.FilterTypeMenuClicked -> {
                _uiState.value = _uiState.value.copy(isTypeMenuOpen = event.expanded)
            }
            is UiEvent.ButtonClicked -> {
                getActivity()
            }
        }
    }

    private fun getActivity() {
        if (_uiState.value.filterParticipantsError) {
            viewModelScope.launch {
                activityEvent.emit(ActivityEvent.Error("Invalid value for participants"))
            }
            return
        }


        _uiState.value = _uiState.value.copy(isLoading = true)

        val type: ActivityType? = if (_uiState.value.filterType == ActivityType.ANY)
            null
        else
            _uiState.value.filterType
        val participants: Int? = _uiState.value.filterParticipants.toIntOrNull()


        viewModelScope.launch(Dispatchers.IO) {
                val call = repo.getFilteredActivity(type, participants)
                call.enqueue(object : Callback<BoredActivity> {
                    override fun onResponse(
                        call: Call<BoredActivity>,
                        response: Response<BoredActivity>
                    ) {
                        if (response.code() == 200) {
                            val newActivity = response.body()!!
                            if (newActivity.error != "") {
                                viewModelScope.launch {
                                    _uiState.value = _uiState.value.copy(isLoading = false)
                                    activityEvent.emit(ActivityEvent.Error(newActivity.error))
                                }
                            } else {
                                viewModelScope.launch {
                                    _uiState.value =
                                        _uiState.value.copy(activity = newActivity, isLoading = false)
                                    activityEvent.emit(ActivityEvent.Success)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<BoredActivity>, t: Throwable) {
                        viewModelScope.launch {
                            _uiState.value = _uiState.value.copy(isLoading = false)
                            activityEvent.emit(ActivityEvent.Error("An Error occurred. Please check your internet connection"))
                        }
                    }
                })
        }
    }
}
