package com.example.boredapi

import com.example.boredapi.model.data.ActivityType
import com.example.boredapi.viewmodel.ActivityViewModel
import com.example.boredapi.viewmodel.UiEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class ViewModelUnitTest {

    private val mainThreadSurrogate = newSingleThreadContext("UI Thread")
    private lateinit var viewModel: ActivityViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = ActivityViewModel()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun viewModel_FilterTypeChange_shouldUpdateState() = runTest {
        viewModel.onEvent(UiEvent.FilterTypeChanged(ActivityType.EDUCATION))
        assertEquals(ActivityType.EDUCATION, viewModel.uiState.value.filterType)
        viewModel.onEvent((UiEvent.FilterTypeChanged(ActivityType.SOCIAL)))
        assertEquals(ActivityType.SOCIAL, viewModel.uiState.value.filterType)
    }

//    @Test
//    fun viewModel_FilterParticipantsChanged_shouldUpdateState_andCheckForError() = runTest {
//        var uiState = viewModel.uiState.value
//        assertEquals("", uiState.filterParticipants)
//        assertFalse(uiState.filterParticipantsError)
//
//        viewModel.onEvent(UiEvent.FilterParticipantsChanged("test"))
//        uiState = viewModel.uiState.value
//        assertEquals("test", uiState.filterParticipants)
//        assertTrue(uiState.filterParticipantsError)
//
//        viewModel.onEvent(UiEvent.FilterParticipantsChanged("2"))
//        uiState = viewModel.uiState.value
//        assertEquals("2", uiState.filterParticipants)
//        assertFalse(uiState.filterParticipantsError)
//    }
}